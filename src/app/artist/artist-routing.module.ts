import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ArtistBrowserComponent} from './artist-browser/artist-browser.component';
import {ArtistComponent} from './artist.component';
import {ArtistViewComponent} from './artist-view/artist-view.component';

const routes: Routes = [
  {path: 'artists', component: ArtistComponent, children: [
      {path: '', redirectTo: 'browse', pathMatch: 'full'},
      {path: 'browse', component: ArtistBrowserComponent},
      {path: 'view/:id', component: ArtistViewComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistRoutingModule { }
