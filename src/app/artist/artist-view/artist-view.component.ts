import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ArtistData, ArtistHttpService, TrackData} from '../artist-http.service';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-artist-view',
  templateUrl: './artist-view.component.html',
  animations: [
    trigger('fadeIn', [
        transition('* => true', [
          style({opacity: 0}),
          animate('250ms ease-in')
        ])
      ]
    ),
    trigger('staggerIn', [
        transition(':enter', [
          query(':enter', [
            style({opacity: 0, height: 0}),
            stagger(40, [
              animate('100ms ease-in', style({height: '100%', opacity: 1})),
            ])
          ], {optional: true})
        ])
      ]
    )
  ]
})
export class ArtistViewComponent implements OnInit {
  @Input() artistId: any;
  artistData$: Observable<ArtistData> | null = null;
  trackData$: Observable<TrackData> | null = null;
  loadState: 'loading' | 'error' | 'complete' = 'loading';
  pictureLoaded = false;

  constructor(private activatedRoute: ActivatedRoute, private artistHttp: ArtistHttpService) { }

  ngOnInit(): void {
    if (this.activatedRoute.snapshot.paramMap.get('id')) {
      // @ts-ignore
      this.artistId = +this.activatedRoute.snapshot.paramMap.get('id');
    }
    this.getArtistDetails();
  }

  private getArtistDetails(): void {
    this.artistData$ = this.artistHttp.getArtist(this.artistId)
      .pipe(
        tap(
          (next) => {
            this.trackData$ = this.artistHttp.getDynamicData(next.tracklist);
          },
          () => {
            this.loadState = 'error';
          },
          () => {
            this.loadState = 'complete';
          },
        )
      );
  }
}
