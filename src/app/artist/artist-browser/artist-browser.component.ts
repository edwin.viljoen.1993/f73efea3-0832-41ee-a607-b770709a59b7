import {Component, OnDestroy, OnInit} from '@angular/core';
import {ArtistHttpService, SearchResponse} from '../artist-http.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {debounceTime, switchMap} from 'rxjs/operators';
import {animate, query, stagger, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-artist-browser',
  templateUrl: './artist-browser.component.html',
  animations: [
    trigger('cardItems', [
      transition('* <=> *', [
        query(':enter', [
          style({
            opacity: 0
          }),
          stagger(100, animate('500ms ease-in'))
        ], {optional: true}),
        query(':leave', [
          animate('500ms ease-out', style({
            opacity: 0
          }))
        ], {optional: true})
      ])
    ]),
    trigger('cardHover', [
      state('hover', style({
        boxShadow: '0 0 3px 6px #08788F3F'
      })),
      transition('* => hover', [
        animate('250ms ease-in')
      ]),
      transition('hover => *', [
        animate('50ms ease-out')
      ])
    ])
  ]
})
export class ArtistBrowserComponent implements OnInit, OnDestroy {
  searchForm: FormGroup = this.fb.group({
    search: [null]
  });

  subs: Subscription[] = [];

  search$: Observable<SearchResponse> | undefined;

  mouseOverCard = -1;

  constructor(private artistHttpService: ArtistHttpService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.artistHttpService.searchTracks('eminem').subscribe(
      (next) => {
        console.log(next);
      }
    );

    this.initSearch();

    // @ts-ignore
    this.subs.push(this.searchForm.get('search').valueChanges.subscribe(x => console.log(x)));
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  private initSearch(): void {
    // @ts-ignore
    this.search$ = this.searchForm.get('search').valueChanges.pipe(
      debounceTime(300),
      switchMap(
        (searchString: string) => this.artistHttpService.searchTracks(searchString)
      )
    );
  }
}
