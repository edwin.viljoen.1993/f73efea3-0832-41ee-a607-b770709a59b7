import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArtistRoutingModule } from './artist-routing.module';
import { ArtistBrowserComponent } from './artist-browser/artist-browser.component';
import { ArtistComponent } from './artist.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ReactiveFormsModule} from '@angular/forms';
import { ArtistViewComponent } from './artist-view/artist-view.component';

@NgModule({
  declarations: [
    ArtistBrowserComponent,
    ArtistComponent,
    ArtistViewComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ArtistRoutingModule,
    FontAwesomeModule
  ]
})
export class ArtistModule { }
