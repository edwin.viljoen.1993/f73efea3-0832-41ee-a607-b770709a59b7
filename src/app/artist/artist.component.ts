import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-artist',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class ArtistComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
