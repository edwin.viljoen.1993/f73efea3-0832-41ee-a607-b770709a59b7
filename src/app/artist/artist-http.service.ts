import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

export interface SearchResponse {
  data: SearchData[];
  total: number;
  next: string;
}

interface SearchData {
  // 	The track's Deezer id	int
  id: string;
  // 	true if the track is readable in the player for the current user	boolean
  readable: boolean;
  // 	The track's fulltitle	string
  title: string;
  // 	The track's short title	string
  title_short: string;
  // 	The track version	string
  title_version: string;
  // 	The url of the track on Deezer	url
  link: string;
  // 	The track's duration in seconds	int
  duration: number;
  // 	The track's Deezer rank	int
  rank: number;
  // 	Whether the track contains explicit lyrics	boolean
  explicit_lyrics: string;
  // 	The url of track's preview file. This file contains the first 30 seconds of the track	url
  preview: string;
  // 	artist object containing : id, name, link, picture, picture_small, picture_medium, picture_big, picture_xl	object
  artist: ArtistData;
  // 	album object containing : id, title, cover, cover_small, cover_medium, cover_big, cover_xl
  album: AlbumData;
}

export interface ArtistData {
  id: number;
  link: string;
  name: string;
  picture: string;
  picture_big: string;
  picture_medium: string;
  picture_small: string;
  picture_xl: string;
  nb_fan: number;
  nb_album: number;
  tracklist: string;
  type: string;
}
export interface TrackData {
  data: any [];
}

interface AlbumData {
  id: number;
  title: string;
  cover: string;
  cover_small: string;
  cover_medium: string;
  cover_big: string;
  cover_xl: string;
}

@Injectable({
  providedIn: 'root'
})
export class ArtistHttpService {

  constructor(private http: HttpClient) { }

  searchTracks(searchString: string): Observable<any> {
    return this.http.get(`${environment.deezerApi}/search`, {params: {q: searchString}});
  }

  getArtist(artistId: number): Observable<any> {
    return this.http.get(`${environment.deezerApi}/artist/${artistId}`);
  }

  getDynamicData(url: string): Observable<any> {
    return this.http.get(url);
  }
}
