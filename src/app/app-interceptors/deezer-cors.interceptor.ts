import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable()
export class DeezerCorsInterceptor implements HttpInterceptor {

  enabled = true;

  deezerUrn = environment.deezerApi;
  proxyUrn = 'https://cors-anywhere.herokuapp.com/';

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.enabled && request.url.startsWith(this.deezerUrn)) {
      return next.handle(request.clone({
        url: this.proxyUrn + request.url
      }));
    }
    return next.handle(request);
  }
}
