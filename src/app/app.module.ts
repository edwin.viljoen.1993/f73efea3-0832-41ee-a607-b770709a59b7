import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ArtistModule} from './artist/artist.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {DeezerCorsInterceptor} from './app-interceptors/deezer-cors.interceptor';
import { NavbarComponent } from './navbar/navbar.component';
import {FaConfig, FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ArtistModule,
    AppRoutingModule,
    FontAwesomeModule
  ],
  providers: [
    [{
      provide: HTTP_INTERCEPTORS,
      useClass: DeezerCorsInterceptor,
      multi: true
    }]
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(faConfig: FaConfig, library: FaIconLibrary) {
    library.addIconPacks(fas, far);
    // faConfig.defaultPrefix = 'far';
    faConfig.fixedWidth = true;
  }
}
